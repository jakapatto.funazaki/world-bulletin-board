from django.apps import AppConfig


class WbbAppConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'wbb_app'
